#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <math.h>

#define WIDTH 16000
#define HEIGHT 9000

#define BOOST_DIST 5000

#define PI 3.14159265

#define ALLY_PODS 2
#define ENEMY_PODS 2

#define CHECKPOINT_RADIUS 600
#define POD_RADIUS 400

#define FRICTION 0.85

#define BOOST 650.0
#define SHIELD -1.0

#define MAX_ROTATION 18.0

#define MAX_THRUST 100.0
#define MAX_VELOCITY 560.0

using namespace std;

class Vector2D
{
public:
    double x;
    double y;
    
    Vector2D() : Vector2D(0.0, 0.0) {}
    Vector2D(double _x, double _y) : x(_x), y(_y) {}
    Vector2D(Vector2D from, Vector2D to) : x(to.x - from.x), y(to.y - from.y) {}
    
    double norm()
    {
        double x2 = x / 1000.0;
        double y2 = y / 1000.0;
        return sqrt(x2 * x2 + y2 * y2) * 1000.0;
    }
    
    Vector2D normalize()
    {
        double vecNorm = norm();
        if (vecNorm == 0.0)
            return Vector2D();
        
        return Vector2D(x / vecNorm, y / vecNorm);
    }
    
    double dist(Vector2D vec)
    {
        return (vec - *this).norm();
    }
    
    double dot(Vector2D vec)
    {
        return x * vec.x + y * vec.y;
    }
    
    Vector2D operator+(Vector2D vec)
    {
        return Vector2D(x + vec.x, y + vec.y);
    }
    
    Vector2D& operator+=(Vector2D vec)
    {
        x += vec.x;
        y += vec.y;
        return *this;
    }
    
    Vector2D operator-(Vector2D vec)
    {
        return Vector2D(x - vec.x, y - vec.y);
    }
    
    Vector2D& operator-=(Vector2D vec)
    {
        x -= vec.x;
        y -= vec.y;
        return *this;
    }
    
    Vector2D operator*(double factor)
    {
        return Vector2D(factor * x, factor * y);
    }
    
    Vector2D& operator*=(double factor)
    {
        x *= factor;
        y *= factor;
        return *this;
    }
    
    Vector2D operator/(double factor)
    {
        return Vector2D(x / factor, y / factor);
    }
    
    Vector2D& operator/=(double factor)
    {
        x /= factor;
        y /= factor;
        return *this;
    }
    
    bool operator==(Vector2D vec)
    {
        return x == vec.x && y == vec.y;
    }
    
    bool operator!=(Vector2D vec)
    {
        return x != vec.x || y != vec.y;
    }
};

class Pod
{
private:
    Vector2D    pos;
    int         angle;
    int         nextCheckpointId;
    Vector2D    velocity;
    Vector2D    target;
    double      thrust;
    bool        canBoost;
public:
    Pod() : pos(Vector2D(0.0, 0.0)), velocity(Vector2D(0.0, 0.0)), target(Vector2D(0.0, 0.0))
    {
        angle = 0;
        thrust = 100;
        nextCheckpointId = 0;
        canBoost = true;
    }
    
    void    read()
    {
        cin >> pos.x >> pos.y >> velocity.x >> velocity.y >> angle >> nextCheckpointId;
        angle = (angle + 360) % 360;
    }
    
    void    setTarget(Vector2D _target)
    {
        target = _target;
    }
    
    void    setThrust(double _thrust)
    {
        thrust = _thrust;
    }
    
    void    print()
    {
        cout << (int)target.x << " " << (int)target.y << " ";
        if (thrust == 650.0 and !canBoost)
            thrust = 100.0;
        if (thrust == 650.0)
        {
            canBoost = false;
            cout << "BOOST" << endl;
        }
        else if (thrust < 0.0)
        {
            cout << "SHIELD" << endl;
        }
        else
            cout << (int)thrust << endl;
    }
    
    Vector2D getTarget()
    {
        return target;
    }
    
    Vector2D targetVec()
    {
        Vector2D targetVec(pos, target);
        
        targetVec.normalize();
        return targetVec;
    }
    
    Vector2D facingVec()
    {
        Vector2D targetVec(cos(angle * PI / 180.0), sin(angle * PI / 180.0));
        
        return targetVec;
    }
    
    Vector2D getVelocity()
    {
        return velocity;
    }
    
    Vector2D getPos()
    {
        return pos;
    }
    
    bool getCanBoost()
    {
        return canBoost;
    }
    
    int getNextCheckpointId()
    {
        return nextCheckpointId;
    }
    
    void follow(Vector2D global_target, double max_thrust, bool canBoost)
    {
        Vector2D desiredVelocity = (global_target - getPos());
        desiredVelocity = desiredVelocity.normalize() * (desiredVelocity.norm() - CHECKPOINT_RADIUS * 1.5);
        Vector2D steering = desiredVelocity - getVelocity();
        Vector2D target = getPos() + steering;
                    
        double thrust = MAX_THRUST;
        if (getVelocity().norm() != 0.0)
        {
            max_thrust =min(max_thrust, steering.norm());
            double angle = acos(facingVec().normalize().dot(steering.normalize()));
            thrust = cos(min(abs(angle / 2.0), 90.0 * PI / 180)) * max_thrust;
        }
        
        if (getPos().dist(target) > BOOST_DIST && thrust >= 95.0 && canBoost)
            thrust = BOOST;
        
        setTarget(target);
        setThrust(thrust);
    }
};

class Path
{
private:
    vector<Vector2D> nodes;
public:
    Path() {}
    
    void addNode(Vector2D node)
    {
        nodes.push_back(node);
    }
    
    Vector2D getNode(int index)
    {
        return nodes[index % nodes.size()];
    }
    
    int length()
    {
        return nodes.size();
    }
};

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
int main()
{
    srand(time(nullptr));
    int laps;
    cin >> laps; cin.ignore();
    int checkpointCount;
    cin >> checkpointCount;
    
    Path path;
    
    Pod allies[ALLY_PODS];
    Pod enemies[ENEMY_PODS];
    
    int x;
    int y;
    for (int i = 0; i < checkpointCount; i++)
    {
        cin >> x >> y;
        path.addNode(Vector2D(x, y));
    }
    
    // game loop
    while (1)
    {
        // Read pods informations
        for (int i = 0; i < ALLY_PODS; i++)
            allies[i].read();
        for (int i = 0; i < ENEMY_PODS; i++)
            enemies[i].read();
                
        // Pods logic
        Pod& runner = allies[0];
        Pod& blocker = allies[1];
        
        Vector2D node = path.getNode(runner.getNextCheckpointId());
        if ((runner.getPos() + runner.getVelocity() * 3.0).dist(node) < CHECKPOINT_RADIUS * 1.0)
            node = path.getNode(runner.getNextCheckpointId() + 1);
        runner.follow(node, MAX_THRUST, true);
        
        Vector2D target = path.getNode(0);
        bool blocking = false;
        for (int e = 0; e < ENEMY_PODS; e++)
        {                
            if (enemies[e].getPos().dist(path.getNode(0)) < 5000.0 && enemies[e].getNextCheckpointId() == 0)
            {
                target = enemies[e].getPos();
                blocking = true;
                break ;
            }
        }

        double max_thrust = 100.0;
        if (!blocking)
        {
            if (blocker.getPos().dist(target) < CHECKPOINT_RADIUS * 1.0)
            {
                target = enemies[0].getPos();
                max_thrust = 0.0;
            }
            else
                max_thrust = 60.0;
        }
        blocker.follow(target, max_thrust, false);
        
        for (int e = 0; e < ENEMY_PODS; e++)
        {
            if (blocker.getPos().dist(enemies[e].getPos()) <= POD_RADIUS * 3.0)
                blocker.setThrust(SHIELD);
        }
        
        runner.print();
        blocker.print();
    }
}